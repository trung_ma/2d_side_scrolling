﻿using System;
using UnityEngine;

public class PooledMono : MonoBehaviour
{
    public event Action<PooledMono> OnBackToPoolEvent;

    public void ReturnToPool()
    {
        if (OnBackToPoolEvent != null)
        {
            OnBackToPoolEvent(this);
        }
    }
}
