﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    public int AmountToPool;

    public PooledMono ObjectToPool;

    public bool ShouldExpand;
}