﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{ 
    [SerializeField]
    ObjectPoolItem[] _objectPoolItems;

    List<PooledMono> _pooledList = new List<PooledMono>();

    void Awake() 
    {
        for (int i = 0; i < _objectPoolItems.Length; i++)
        {
            ObjectPoolItem poolItem = _objectPoolItems[i];

            for (int j = 0; j < poolItem.AmountToPool; j++)
            {
                CreateObject(poolItem.ObjectToPool);
            }
        }
    }

    public PooledMono Get(bool enable = true)
    {
        return Get<PooledMono>(enable);
    }

    public T Get<T>(bool enable = true) where T : PooledMono
    {
        for (int i = 0; i < _pooledList.Count; i++)
        {
            PooledMono pooledObject = _pooledList[i];
            if(pooledObject is T && !pooledObject.gameObject.activeInHierarchy)
            {
                pooledObject.gameObject.SetActive(enable);
                return pooledObject as T;
            }
        }

        for (int i = 0; i < _objectPoolItems.Length; i++)
        {
            ObjectPoolItem poolItemConfig = _objectPoolItems[i];
            if(poolItemConfig.ObjectToPool is T && poolItemConfig.ShouldExpand)
            {
                PooledMono pooledObject = CreateObject(poolItemConfig.ObjectToPool);
                pooledObject.gameObject.SetActive(enable);
                return pooledObject as T;
            }
        }

        return null;
    }

    public T Get<T>(Transform parent, bool resetTransform = false) where T : PooledMono
    {
        T pooledObject = Get<T>(true);
        if(pooledObject == null)
        {
            Debug.LogError(typeof(T).ToString() + " type does NOT exist in pool");
            return null;
        }

        pooledObject.transform.SetParent(parent);
        if (resetTransform)
        {
            pooledObject.transform.localPosition = Vector3.zero;
            pooledObject.transform.localRotation = Quaternion.identity;
        }

        return pooledObject;
    }

    PooledMono CreateObject(PooledMono objectToPool)
    {
        PooledMono pooledItem = Instantiate(objectToPool);
        pooledItem.OnBackToPoolEvent += ReturnToPool;
        pooledItem.transform.SetParent(transform);
        pooledItem.gameObject.SetActive(false);
        _pooledList.Add(pooledItem);
        return pooledItem;
    }

    public void ReturnToPool(PooledMono objectToReturn)
    {
        objectToReturn.gameObject.SetActive(false);
        objectToReturn.transform.SetParent(transform);
        objectToReturn.transform.position = Vector3.zero;
        objectToReturn.transform.rotation = Quaternion.identity;
    }
}
