﻿using UnityEngine;
using System.Collections;

public abstract class MonoSingleton<T> : MonoBehaviour where T: MonoSingleton<T>
{
	private static T _instance = null;

	public static T Instance
	{
		get { return _instance; }
	}

	void Awake()
	{
		_instance = this as T;
		_instance.OnAwake();
	}

	protected virtual void OnAwake()
	{		
	}

	protected virtual void OnDestroy()
	{
		_instance = null;
	}

	private void OnApplicationQuit()
	{
		#if !UNITY_EDITOR
		g_instance = null;
		#endif
	}
}