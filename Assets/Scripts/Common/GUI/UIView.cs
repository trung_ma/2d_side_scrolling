﻿using UnityEngine;
using System;
[RequireComponent(typeof(CanvasGroup))]
public abstract class UIView : MonoBehaviour
{
    public string UIName { get; set; }

    public bool IsLoadingView { get; set; }

    public event Action OnHideComplete;

    CanvasGroup _canvasGroup;

    bool _isShow = false;

    void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public virtual void Show()
    {

    }

    public virtual void Present()
    {
        EnableView(true);
    }

    public virtual void Hide(bool isDestroy)
    {
        OnHideViewComplete(isDestroy);
    }
    
    public void DestroyThisView()
    {
        if(gameObject != null)
        {
            Destroy(this.gameObject);
        }
    }

    void OnHideViewComplete(bool isDestroy)
    {
        EnableView(false);
        if(isDestroy){
            transform.parent = null;
            DestroyThisView();
        }

        if(OnHideComplete != null)
        {
            OnHideComplete();
        }
    }

    void EnableView(bool isEnable)
    {
        _canvasGroup.alpha = isEnable ? 1 : 0;
        _canvasGroup.interactable = isEnable;
        _canvasGroup.blocksRaycasts = isEnable;
        _isShow = isEnable;
    }
}

