﻿using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private UIViewStack _uiViewStack;

    private Dictionary<string, string> _dictGUIName = new Dictionary<string, string>();

    private List<UIView> _viewList = new List<UIView>();

    public struct UIName
    {
        public const string UIMainMenu = "UIMainMenu";
        public const string UIMainGame = "UIMainGame";
        public const string UISettings = "UISettings";
        public const string UIGameOver = "UIGameOver";
    }

    protected override void OnAwake()
    {
        _dictGUIName.Add(UIManager.UIName.UIMainMenu, "UIMainMenu");
        _dictGUIName.Add(UIManager.UIName.UIMainGame, "UIMainGame");
        _dictGUIName.Add(UIManager.UIName.UISettings, "UISettings");
        _dictGUIName.Add(UIManager.UIName.UIGameOver, "UIGameOver");
    }

    public UIView Show(string viewName, bool isLoadingView = false)
    {
        if(_dictGUIName.ContainsKey(viewName))
        {
            UIView view = GetCache(viewName);
            if(view == null)
            {
                view = GetGUI(viewName);
                view.UIName = viewName;
                view.IsLoadingView = isLoadingView;
            }

            _viewList.Add(view);
            _uiViewStack.PushView(view);
            view.Show();

            return view;
        }

        return null;
    }

    public void Hide (string viewName, bool isDestroy = false)
    {
        UIView view = GetCache(viewName);
        if(isDestroy)
        {
            _viewList.Remove(view);
        }

        view.Hide(isDestroy);
    }

    public void HideAll ()
    {
        for(int i = 0; i < _viewList.Count; i++)
        {
            UIView view = _viewList[i];
            if(view != null && view.gameObject != null)
            {
                view.DestroyThisView();
            }
        }

        _viewList.Clear();
    }

    public UIView GetCache(string guiName)
    {
        return _viewList.Find(view => view.UIName == guiName);
    }

    UIView GetGUI(string guiName)
    {
        StringBuilder builder = new StringBuilder();
        string value = builder.Append("GUI/").Append(_dictGUIName[guiName]).ToString();
        //Debug.Log("Load " + value);

        // We will Change to load prefab from asset bundle in the future.
        GameObject goInstance = Instantiate(Resources.Load(value,  typeof(GameObject))) as GameObject;
        return goInstance.GetComponent<UIView>();
    }
}


