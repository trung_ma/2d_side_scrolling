﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkService : MonoBehaviour
{
    public void SendGetRequest(string url, Action<bool, string> callback)
    {
        StartCoroutine(SendGet(url, callback));
    }

    public void SendPostRequest(string url, string data, Action<bool, string> callback)
    {
        SendRequest(url, data, UnityWebRequest.kHttpVerbPOST, callback);
    }

    IEnumerator SendGet(string url, Action<bool, string> callback)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(url);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            if (callback != null)
            {
                callback(false, uwr.error);
            }
        }
        else
        {
            if (callback != null)
            {
                callback(true, uwr.downloadHandler.text);
            }
        }
    }

    public void SendRequest(string url, string data, string method, Action<bool, string> callback)
    {
        StartCoroutine(Send(url, data, method, callback));
    }

    IEnumerator Send(string url, string data, string method, Action<bool, string> callback)
    {
        UnityWebRequest webRequest = new UnityWebRequest
        {
            url = url
        };

        if (method.Equals(UnityWebRequest.kHttpVerbPOST))
        {
            if (data == null)
            {
                Debug.LogError("Cannot request null data.,");
                yield break;
            }

            byte[] body = System.Text.Encoding.UTF8.GetBytes(data);
            UploadHandler uploader = new UploadHandlerRaw(body);
            webRequest.uploadHandler = uploader;
            webRequest.downloadHandler = new DownloadHandlerBuffer();
            webRequest.SetRequestHeader("Content-Type", "application/json");
        }

        webRequest.method = method;
        webRequest.chunkedTransfer = false;
        webRequest.timeout = 60;

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
            if(callback != null)
            {
                callback(false, webRequest.error);
            }
        }
        else
        {
            if(callback != null)
            {
                callback(true, webRequest.downloadHandler.text);
            }
        }

        webRequest.Dispose();
    }
}
