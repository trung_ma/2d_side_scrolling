﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackgound : MonoBehaviour
{
    [SerializeField]
    float _scrollingSpeed;

    MeshRenderer _meshRenderer;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

	// Update is called once per frame
	void Update ()
    {
        if(!GameManager.Instance.IsIplaying)
        {
            return;
        }

        Vector2 offset = new Vector2(Time.time * _scrollingSpeed, 0); 
        _meshRenderer.material.mainTextureOffset = offset;

    }
}
