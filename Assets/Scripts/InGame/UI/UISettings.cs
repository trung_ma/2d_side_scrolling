﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISettings : UIView
{
    private void Start()
    {
        Time.timeScale = 0;
    }

    public void OnResumeButtonClicked()
    {
        Time.timeScale = 1;
        Hide(true);
    }

    public void OnQuitButtonClicked()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}
