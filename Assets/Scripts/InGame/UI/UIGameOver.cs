﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOver : UIView
{
    [SerializeField]
    Text _scoreText;

    public event Action OnRetryButtonClickedAction;

    public void SetScore(int score)
    {
        _scoreText.text = score.ToString();
    }

    public void OnRetryButtonClicked()
    {
        if(OnRetryButtonClickedAction != null)
        {
            OnRetryButtonClickedAction();
        }
    }
	
}
