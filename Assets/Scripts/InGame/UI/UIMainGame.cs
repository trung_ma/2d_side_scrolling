﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainGame : UIView
{
    [SerializeField]
    Text _scoreText;

    [SerializeField]
    Text _highscoreText;

    private void Start()
    {
        GameManager.Instance.OnPointChanged += Handle_OnPointChanged;
    }

    private void Handle_OnPointChanged(int score)
    {
        _scoreText.text = score.ToString();
    }

    public void SetHighscore(int score)
    {
        _highscoreText.text = score.ToString();
    }

    public void OnSettingButtonClicked()
    {
        UIManager.Instance.Show(UIManager.UIName.UISettings);
    }
}
