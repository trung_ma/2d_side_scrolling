﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenu : UIView
{
    public event Action OnStartButtonClickedAction;

    public void OnStartButtonClicked()
    {
        if(OnStartButtonClickedAction != null)
        {
            OnStartButtonClickedAction();
        }
    }

    public void OnQuitButtonClicked()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

}
