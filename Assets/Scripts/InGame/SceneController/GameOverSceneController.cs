﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverSceneController : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        UIGameOver uiGameOver = (UIGameOver) UIManager.Instance.Show(UIManager.UIName.UIGameOver);
        if(uiGameOver != null)
        {
            uiGameOver.SetScore(DataManager.Instance.CurrentScore);
            uiGameOver.OnRetryButtonClickedAction += Handle_OnRetryButtonClickedAction;
        }
    }

    private void Handle_OnRetryButtonClickedAction()
    {
        SceneController.Instance.LoadScene(SceneName.Game);
    }

    private void OnDisable()
    {
        if(UIManager.Instance != null)
        {
            UIManager.Instance.HideAll();
        }
    }
}
