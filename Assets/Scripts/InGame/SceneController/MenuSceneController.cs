﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSceneController : MonoBehaviour
{
    private void Start()
    {
        UIMainMenu uiMainMenu = (UIMainMenu) UIManager.Instance.Show(UIManager.UIName.UIMainMenu);
        if(uiMainMenu != null)
        {
            uiMainMenu.OnStartButtonClickedAction += Handle_OnStartButtonClickedAction;            
        }
    }

    private void Handle_OnStartButtonClickedAction()
    {
        SceneController.Instance.LoadScene(SceneName.Game);
    }

    private void OnDisable()
    {
        if(UIManager.Instance != null)
        {
            UIManager.Instance.HideAll();
        }
    }
}
