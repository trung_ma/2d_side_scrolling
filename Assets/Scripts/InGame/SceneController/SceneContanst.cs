﻿public struct SceneName
{
    public const string Game = "Game";
    public const string Menu = "Menu";
    public const string GameOver = "GameOver";
}
