﻿using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoSingleton<SceneController> 
{
    string _currentScene;

    protected override void OnAwake()
    {
        _currentScene = string.Empty;
        LoadScene(SceneName.Menu);
    }

    public void LoadScene(string sceneName)
    {
        //StartCoroutine(UnloadSceneAsync(_currentScene));
        StartCoroutine(Load(sceneName));
    }

    IEnumerator Load(string sceneName)
    {
        yield return StartCoroutine(UnloadSceneAsync(_currentScene));
        yield return StartCoroutine(LoadSceneAsync(sceneName));
    }

    private IEnumerator UnloadSceneAsync(string sceneName)
    {
        Scene scene = SceneManager.GetSceneByName(sceneName);
        if(scene.isLoaded)
        {
            yield return SceneManager.UnloadSceneAsync(sceneName);
        }
    }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        _currentScene = sceneName;
    }
}
