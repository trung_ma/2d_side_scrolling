﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneController : MonoBehaviour
{
    [SerializeField]
    GameManager _gameManager;

    private void Awake()
    {
        UIManager.Instance.Show(UIManager.UIName.UIMainGame);
    }
    
    // Use this for initialization
    void Start ()
    {
        _gameManager.StartGame();
    }

    private void OnDisable()
    {
        if (UIManager.Instance != null)
        {
            UIManager.Instance.HideAll();
        }
    }

}
