﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour 
{
    public Obstacle CurrentTrackingObstacle
    {
        get
        {
            if(_currentTrackingObstacle == null)
            {
                _currentTrackingObstacle = _obstacles[0];
            }

            return _currentTrackingObstacle;
        }
    }

    Obstacle _currentTrackingObstacle;

    readonly List<Obstacle> _obstacles = new List<Obstacle>();

    SegmentManager _segmentManager;

    const int MAX_OBSTACLE_SPWANED_EACH_TIME = 2;

    [SerializeField]
    ObjectPooler[] _poolers;

    private void Awake()
    {
        _segmentManager = FindObjectOfType<SegmentManager>();
        _segmentManager.OnNewSegmentHasSpawn += Handle_OnNewSegmentHasSpawn;
        _segmentManager.OnSegmentRemoved += Handle_OnSegmentRemoved;
    }

    void Handle_OnNewSegmentHasSpawn(Segment segment)
    {
        SpawnObstacle(segment);
    }

    void Handle_OnSegmentRemoved(Segment segment)
    {
        for (int i = 0; i < _obstacles.Count; i++)
        {
            Obstacle obstacle = _obstacles[i];
            if(obstacle.Owner.gameObject == segment.gameObject)
            {
                obstacle.ReturnToPool();
                _obstacles.Remove(obstacle);
                i--;
            }
        }
    }

    public void SpawnObstacle(Segment segment)
    {
        int obstacleCount = Random.Range(1, segment.ObstaclePositions.Length + 1);

        for (int i = 0; i < obstacleCount; i++)
        {
            int obstacleType = Random.Range(0, _poolers.Length);
            Obstacle obstacle = _poolers[obstacleType].Get<Obstacle>(segment.transform, resetTransform: true);
            Vector3 position = GetRandomPosition(segment, i);
            obstacle.Owner = segment;
            obstacle.Setup(position);
            _obstacles.Add(obstacle);
        }
    }

    Vector2 GetRandomPosition(Segment segment, int index)
    {
        Vector3 startPosition = segment.ObstaclePositions[index].GetChild(0).transform.position;
        Vector3 endPosition = segment.ObstaclePositions[index].GetChild(1).transform.position;
        float x = Random.Range(startPosition.x, endPosition.x);
        Vector3 position = segment.transform.position;
        position.x = x;

        return position;
    }

    public void SetNextObstacleTracking(Obstacle obstacle)
    {
        int index = GetObstacleIndex(obstacle);
        if(index != -1 && index < _obstacles.Count - 1)
        {
            index++;
            _currentTrackingObstacle = _obstacles[index];
        }
        else
        {
            Debug.LogError("Cannot find obstacle!");
        }
    }

    int GetObstacleIndex(Obstacle obstacle)
    {
        for (int i = 0; i < _obstacles.Count; i++)
        {
            if(_obstacles[i].gameObject == obstacle.gameObject)
            {
                return i;
            }
        }

        return -1;
    }
}
