﻿using UnityEngine;

public class Obstacle : PooledMono
{
    public float Length = 0.8f;

    [SerializeField]
    protected GameObject _playerDeathPrefab;

    [SerializeField]
    protected float _yPosition = 1f;

    public Segment Owner { get; set; }

    private void OnDisable()
    {
        Owner = null;
    }

    public virtual void Setup(Vector2 position)
    {
        position.y = _yPosition;
        transform.position = position;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        IAttackable target = collision.gameObject.GetComponent<IAttackable>();
        if(target != null && Owner != null && target.CanTakeDamage)
        {
            target.TakeDamage();
            if(_playerDeathPrefab != null)
            {
                ParticleSystem particle = Instantiate(_playerDeathPrefab, collision.gameObject.transform).GetComponent<ParticleSystem>();
                particle.transform.localPosition = Vector3.zero;
                particle.Play();
            }
        }
    }

}
