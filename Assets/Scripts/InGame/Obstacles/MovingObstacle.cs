﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis
{
    X, Y
}

public class MovingObstacle : Obstacle
{
    [SerializeField]
    Axis _axis;

    [SerializeField]
    float _maxDisplacement;

    [SerializeField]
    float _moveSpeed;

    Vector2 _currentDirection;

    Vector3 _targetPosition;

    Vector3 _startPosition;

    Vector3 _maxDisplacementPosition;

    bool _isMove;

    public override void Setup(Vector2 position)
    {
        _isMove = false;
        position.y = _yPosition;
        transform.position = _maxDisplacementPosition = _startPosition = position;

        switch (_axis)
        {
            case Axis.X:
                _currentDirection = Vector2.right;
                _maxDisplacementPosition.x += _maxDisplacement;
                break;

            case Axis.Y:
                _currentDirection = Vector2.up;
                _maxDisplacementPosition.y += _maxDisplacement;
                break;
        }

        SetTarget();
        _isMove = true;
    }

    private void Update()
    {
        if (!_isMove)
        {
            return;
        }

        Vector3 displacement = _currentDirection * _moveSpeed * Time.deltaTime;
        transform.Translate(displacement);

        if (Vector3.Distance(transform.position, _targetPosition) < 0.05f)
        {
            _currentDirection *= -1f;
            SetTarget();
        }
    }

    void SetTarget()
    {
        if (_targetPosition.CompareTo(_maxDisplacementPosition))
        {
            _targetPosition = _startPosition;
        }
        else
        {
            _targetPosition = _maxDisplacementPosition;
        }
    }

    private void OnDrawGizmos()
    {
        if(Application.isPlaying)
        {
            return;
        }

        Gizmos.color = Color.red;
        Vector3 pos = transform.position;
        switch (_axis)
        {
            case Axis.X:
                _currentDirection = Vector2.right;
                pos.x += _maxDisplacement;
                break;

            case Axis.Y:
                _currentDirection = Vector2.up;
                pos.y += _maxDisplacement;
                break;
        }

        Gizmos.DrawLine(transform.position, pos);
        Gizmos.DrawCube(pos, Vector3.one * 0.2f);
    }
}
