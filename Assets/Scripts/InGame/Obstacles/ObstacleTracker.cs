﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTracker : MonoBehaviour
{
    ObstacleManager _obstacleManager;

    PlayerGrounding _playerGrounding;

    private void Awake()
    {
        _obstacleManager = GetComponent<ObstacleManager>();
        _playerGrounding = FindObjectOfType<PlayerGrounding>();
    }

    private void Update()
    {
        Obstacle currentObstacle = _obstacleManager.CurrentTrackingObstacle;
        float threshold = currentObstacle.transform.position.x + currentObstacle.Length;
        if (_playerGrounding.transform.position.x > threshold
                && _playerGrounding.IsOnGround())
        {
            _obstacleManager.SetNextObstacleTracking(_obstacleManager.CurrentTrackingObstacle);
            GameManager.Instance.AddPoint();
        }
    }
}
