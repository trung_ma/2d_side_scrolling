﻿using UnityEngine;

public static class Vector3Ext
{
    const float MIN_VALUE = 0.05f;

    public static bool CompareTo(this Vector3 vec, Vector3 another)
    {
        return Vector3.SqrMagnitude(vec - another) < MIN_VALUE;
    }
}
