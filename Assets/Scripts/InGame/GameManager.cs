﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    [SerializeField]
    PlayerController _playerController;

    [SerializeField]
    float _delayTimeToChangeScene = 0.5f;

    [SerializeField]
    int _scoreJumpOver = 10;

    public event Action<int> OnPointChanged = delegate { };

    public bool IsIplaying { get; set; }

    protected override void OnAwake()
    {
        _playerController.OnPlayerDied += Handle_OnPlayerDied;
    }

    private void Handle_OnPlayerDied()
    {
        EndGame();
    }

    public void StartGame()
    {
        DataManager.Instance.CurrentScore = 0;
        IsIplaying = true;
        _playerController.Begin();

        UIMainGame uiMainGame = (UIMainGame) UIManager.Instance.GetCache(UIManager.UIName.UIMainGame);
        if(uiMainGame != null)
        {
            PlayerData playerData = DataManager.Instance.GetPlayerData();
            uiMainGame.SetHighscore(playerData.Score);
        }
    }

    public void EndGame()
    {
        if(!IsIplaying)
        {
            return;
        }

        IsIplaying = false;
        PlayerData playerData = DataManager.Instance.GetPlayerData();

        if(DataManager.Instance.CurrentScore > playerData.Score)
        {
            playerData.Score = DataManager.Instance.CurrentScore;
            DataManager.Instance.SetPlayerData(playerData);
        }

        PlayerData data = new PlayerData();
        data.UserName = playerData.UserName;
        data.Score = DataManager.Instance.CurrentScore;
        SendLeaderboard(data);

        StartCoroutine(ChangeToGameOverScene());
    }

    void SendLeaderboard(PlayerData playerData)
    {
        var serializedData = JsonConvert.SerializeObject(playerData);
        APIService.Instance.SendRequestLeaderboard(serializedData);
    }

    IEnumerator ChangeToGameOverScene()
    {
        yield return new WaitForSeconds(_delayTimeToChangeScene);
        SceneController.Instance.LoadScene(SceneName.GameOver);
    }

    public void AddPoint()
    {
        if(OnPointChanged != null && IsIplaying)
        {
            DataManager.Instance.CurrentScore += _scoreJumpOver;
            OnPointChanged(DataManager.Instance.CurrentScore);
        }
    }
}
