﻿public class PlayerData
{
    public string UserName { get; set; }

    public int Score { get; set; }
}
