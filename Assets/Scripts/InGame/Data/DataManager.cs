﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoSingleton<DataManager>
{

    PlayerData _playerData;

    public int CurrentScore { get; set; }

    private void Start()
    {
        _playerData = new PlayerData();
        _playerData.UserName = PlayerPrefs.GetString("username", "player_01");
        _playerData.Score = PlayerPrefs.GetInt("scrore", 0);
    }

    public PlayerData GetPlayerData()
    {
        return _playerData;
    }

    public void SetPlayerData(PlayerData playerData)
    {
        PlayerPrefs.SetString("username", playerData.UserName);
        PlayerPrefs.SetInt("scrore", playerData.Score);
    }
}
