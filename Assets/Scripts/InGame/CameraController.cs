﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
	PlayerController _playerController;

	Vector3 _previousPositiion;

	// Use this for initialization
	void Awake () 
	{
		_playerController = FindObjectOfType<PlayerController>();
		_previousPositiion = _playerController.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		FollowPlayer();
	}

	void FollowPlayer()
	{
		Vector3 position = transform.position;
		position.x += _playerController.transform.position.x - _previousPositiion.x;
		transform.position = position;
		_previousPositiion = _playerController.transform.position;
	}
}
