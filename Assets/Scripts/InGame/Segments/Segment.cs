﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : PooledMono 
{
    public Transform[] ObstaclePositions;

	[SerializeField]
	float _segmentLength = 8;

	public virtual float Length 
	{
		get { return _segmentLength; }
	}
}
