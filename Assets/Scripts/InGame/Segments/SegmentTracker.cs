﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SegmentManager))]
public class SegmentTracker : MonoBehaviour
{
    [SerializeField]
    Transform _playerTransform;

    SegmentManager _segmentManager;

    const float SAFE_ZONE = 5;

    private void Awake()
    {
        _segmentManager = GetComponent<SegmentManager>();
    }

    // Update is called once per frame
    void Update ()
    {
        float threshold = _segmentManager.SpawnZPositon - _segmentManager.SegmentCount * _segmentManager.FirstSegment.Length;
        float currentPosition = _playerTransform.position.x - SAFE_ZONE;

        if (currentPosition > threshold)
        {
            _segmentManager.HandlePlayerPassedSegment();
        }
    }
}
