﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentManager : MonoBehaviour
{
	[SerializeField]
	float _segmentCount = 10;

	[SerializeField]
	Transform _segmentHolder;

    [SerializeField]
    ObjectPooler _pooler;

    public float SegmentCount { get { return _segmentCount; } }

    public event Action<Segment> OnNewSegmentHasSpawn;

    public event Action<Segment> OnSegmentRemoved;

    public float SpawnZPositon { get; set; }

    public Segment FirstSegment { get { return _segments[0]; }}

	public Segment LastSegment { get { return _segments[_segments.Count - 1]; } }

    readonly List<Segment> _segments = new List<Segment>();

    void Start()
	{
		for (int i = 0; i < _segmentCount; i++)
        {
            bool isSendEvent = i > 1;
            SpawnNewSegment(isSendEvent);
        }
	}

	void SpawnNewSegment(bool isSendEvent = true)
    {
		Segment segment = _pooler.Get<Segment>(_segmentHolder, true);
		segment.transform.localPosition = Vector2.right * SpawnZPositon;
        SpawnZPositon += segment.Length;
		_segments.Add(segment);

        if(isSendEvent && OnNewSegmentHasSpawn != null)
        {
            OnNewSegmentHasSpawn(segment);
        }
    }

	void RemoveSegment()
	{
        Segment segment = FirstSegment;

        if (OnSegmentRemoved != null)
        {
            OnSegmentRemoved(segment);
        }

        segment.ReturnToPool();
        _segments.Remove(segment);
    }

    public void HandlePlayerPassedSegment()
    {
        SpawnNewSegment();
        RemoveSegment();
    }
}
