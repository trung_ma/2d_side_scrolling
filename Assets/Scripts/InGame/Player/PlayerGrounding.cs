﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrounding : MonoBehaviour 
{
	[SerializeField]
	Transform _playerFeet;

	[SerializeField]
	LayerMask _groundMask;

	public bool IsOnGround()
	{
		RaycastHit2D[] raycastHits = Physics2D.RaycastAll(_playerFeet.position, Vector2.down, 0.2f, (int) _groundMask);

		if(raycastHits.Length > 0)
		{
			Debug.DrawRay(_playerFeet.position, Vector2.down * 0.5f, Color.green);
		}
		else
		{
			Debug.DrawRay(_playerFeet.position, Vector2.down * 0.5f, Color.black);
		}

		return raycastHits.Length > 0;
	}
}
