﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IAttackable
{
	[SerializeField]
	float _minSpeed;

	[SerializeField]
	float _maxSpeed;

    [SerializeField]
    SpriteRenderer _spriteRenderer;

    [SerializeField]
    float _timeCounterIncreaseSpeed;

    public event Action OnPlayerDied;

    public bool IsPlaying { get; set; }

    public bool CanTakeDamage
    {
        get
        {
            return IsPlaying;
        }
    }

    float _currentSpeed;

	Rigidbody2D _rigidbody2D;

    float _lastTimestamp;

	void Awake()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
        _currentSpeed = _minSpeed;
    }

	public void Begin()
	{
		IsPlaying = true;
        _lastTimestamp = Time.time;
    }

    void Update()
	{
		if(!IsPlaying)
		{
			return;
		}

		Move();
        IncreaseSpeed();
	}

    void IncreaseSpeed()
    {
        if(Time.time - _lastTimestamp > _timeCounterIncreaseSpeed)
        {
            _lastTimestamp = Time.time;
            _currentSpeed = Mathf.Clamp(_currentSpeed + 0.25f, _minSpeed, _maxSpeed);
        }
    }

    void Move()
	{
		Vector2 velocity = Vector3.right * _currentSpeed;
		velocity.y = _rigidbody2D.velocity.y;
		_rigidbody2D.velocity = velocity;
	}

    public void TakeDamage()
    {
        Die();

        if (OnPlayerDied != null)
        {
            OnPlayerDied();
        }
    }

    public void Die()
    {
        IsPlaying = false;
        _spriteRenderer.enabled = false;
        _rigidbody2D.velocity = Vector3.zero;
        _currentSpeed = 0;
    }
}
