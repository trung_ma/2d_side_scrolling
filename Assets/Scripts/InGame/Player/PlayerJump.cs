﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerGrounding))]
public class PlayerJump : MonoBehaviour 
{

	[SerializeField]
	float _jumpForce = 10;

	[SerializeField]
	KeyCode _jumpKey;

	Rigidbody2D _rigidbody2D;
	
	PlayerGrounding _playerGrounding;

	PlayerController _playerController;

    void Awake()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		_playerGrounding = GetComponent<PlayerGrounding>();
        _playerController = GetComponent<PlayerController>();
    }

	void Update () 
	{
		if(ReadInput() && _playerGrounding.IsOnGround() && _playerController.IsPlaying)
		{
			Jump();
		}
	}

	void Jump()
	{
		_rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x , _jumpForce);
	}

	bool ReadInput()
	{
		return Input.GetKey(_jumpKey) || (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == null);
	}
}
