﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackable
{
    bool CanTakeDamage { get; }

    void TakeDamage();
}
