﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APIService :  MonoSingleton<APIService>
{
    const string END_POINT = "https://example/";

    NetworkService _networkService;

    private void OnEnable()
    {
        _networkService = GetComponent<NetworkService>();
    }

    public void SendRequestLeaderboard(string data)
    {
        string url = END_POINT + "leaderboard";
        _networkService.SendPostRequest(url, data, (susscess, responeData) =>
        {
            if (susscess)
            {
                Debug.Log(data);
            }
        });
    }
}
